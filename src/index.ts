import { Hono } from 'hono'
import { serveStatic } from 'hono/bun'
import { Router as IttyRouter } from 'itty-router'

const app = new Hono()

// Create itty-router application
const ittyRouter = IttyRouter()

app.use('/static/*', serveStatic({ root: './' }))
app.use('/favicon.ico', serveStatic({ path: './favicon.ico' }))
app.get('/', (c) => c.text('You can access: /static/hello.txt'))
app.get('*', serveStatic({ path: './static/fallback.txt' }))
app.notFound((c) => {
    return c.text('Custom 404 Message', 404)
})
app.onError((err, c) => {
    console.log(`${err}`)
    return c.text(`Custom Error Message`, 500)
})

ittyRouter.get('/hello', () => new Response('Hello from itty-router'))
app.mount('/itty-router', ittyRouter.handle)
app.post('/', (c) => c.text(`GET /`))

// Any HTTP methods
app.all('/hello', (c) => c.text('Any Method /hello'))

// Custom HTTP method 
app.on('PURGE', '/cache', (c) => c.text('PURGE Method /cache'))



export default app
